'use strict';

$('#spinnerOverlay, #spinner, #spinnerMessage').show();

/* global instantsearch */
function AlgoliaSearch() {
}

AlgoliaSearch.prototype = {
    appId: 'W8W14RA37V',
    apiKey: 'MmFhNzJlMDE4N2YyYTc2OWFiN2IzZmNjNmI2MDFhNDM3OWY2MWYwYWRmZDJkZTkwNjY4ZmRkOWNkNmU0Zjg4MHRhZ0ZpbHRlcnM9LVdBSVRJTkcsSEFTSEFORExF',
    indexName: 'products_custom',
    caracteristics: null,
    search: null,
    products: [],
    toggleTimer: null,
    facetTemplateCheckbox:
        '<div class="item">'+
        '    <input type="checkbox" class="css-checkbox {{cssClasses.checkbox}}" value="{{name}}" {{#isRefined}}checked{{/isRefined}}>'+
        '    <label class="css-label">{{name}} ({{count}})</label>'+
        '</div>',
    menuTemplate:
        '<a href="javascript:void(0);" class="facet-item {{#isRefined}}categorieSelected{{/isRefined}}">'+
        '    <span class="facet-name">{{name}} ({{count}})</span class="facet-name">'+
        '</a>',
    hitTemplateSmall: $('#templateHitSmall').html().replace(new RegExp('\##\\[', 'g'), '{{{').replace(new RegExp('\\]##', 'g'),'}}}'),
    hitTemplateMedium: $('#templateHitMedium').html().replace(new RegExp('\##\\[', 'g'), '{{{').replace(new RegExp('\\]##', 'g'),'}}}'),
    hitTemplateBig: $('#templateHitBig').html().replace(new RegExp('\##\\[', 'g'), '{{{').replace(new RegExp('\\]##', 'g'),'}}}'),
    noResultsTemplate:
        '<div class="text-center">No results found matching <strong>{{query}}</strong>.</div>'

};

AlgoliaSearch.prototype.changeIndex = function(indexName) {
    this.indexName = indexName;
    this.search.indexName = this.indexName;
}

AlgoliaSearch.prototype.init = function() {
    this.search = instantsearch({
        appId: this.appId,
        apiKey: this.apiKey,
        indexName: this.indexName,
        urlSync: true
    });
    this.initSearch();
    this.initFacets();
}

AlgoliaSearch.prototype.initFacets = function() {
    this
        .getCaracteristics()
        .then(jQuery.proxy(this.getAttributesForFaceting, this))
        .then(jQuery.proxy(this.showFacets,this))
    ;

    this.search.addWidget(
        instantsearch.widgets.hierarchicalMenu({
            container: '#categories',
            attributes: [
                'categories.en.TREE_0',
                'categories.en.TREE_1',
                'categories.en.TREE_2',
                'categories.en.TREE_3',
                'categories.en.TREE_4',
                'categories.en.TREE_5',
                'categories.en.TREE_6',
                'categories.en.TREE_7'
            ],
            sortBy: ['name:asc'],
            cssClasses: {
              active: 'categorieSelected'
            },
            templates: {
              item: this.menuTemplate
            },
            showParentLevel: true,
            autoHideContainer: false,
            separator: ' > ',
            transformData: function(str) {
                var max = 20;
                var sep = '...';
                var seplen = 3;

                var len = str.length;
                if(len > max){
                    if(seplen > max) { return str.substr(len - max) }

                    var n = -0.5 * (max - len - seplen);
                    var center = len/2;
                    return str.substr(0, center - n) + sep + str.substr(len - center + n);
                }
                return str;
            }
        })
    );
}

AlgoliaSearch.prototype.showFacets = function(facets) {
    var pattern = 'caracteristics.en.';

    for (var i = 0; i < facets.length; i++) {
        var facet = facets[i];
        if (facet.indexOf(pattern) > -1) {
            var id = facet.replace(/\./g,'_');
            var title = this.caracteristics[facet.replace(pattern,'')].name
            var newDiv =
                '<div class="facetFilter facetContainerToggle filtre">'+
                '    <div class="titre facetToggle">'+
                '        <div><h3 class="ng-binding">'+title+'</h3></div>'+
                '        <div id="metal-fleche" class="ferme">&nbsp;</div>'+
                '    </div>'+
                '    <div class="contenu" style="display:none;" ng-show="showChoices">'+
                '        <div class="choix">'+
                '           <div id="'+id+'" class="checkboxs">'+
                '           </div>'+
                '        </div>'+
                '    </div>'+
                '</div>'
            ;

            $('#facetsContainer .facetFilter').last().after($(newDiv));

            this.search.addWidget(
                instantsearch.widgets.refinementList({
                    container: '#'+id,
                    attributeName: facet,
                    operator: 'or',
                    limit: 100,
                    cssClasses: {
                        active: 'active'
                    },
                    templates: {
                        item: this.facetTemplateCheckbox,
                        header: ''
                    }
                })
            );
        }
    }

    this.search.start();
}

AlgoliaSearch.prototype.initHit = function (mode) {

    var that = this;

    this.search.addWidget(
        instantsearch.widgets.hits({
            container: '#resultsContainer',
            hitsPerPage: 24,
            templates: {
                empty: that.noResultsTemplate,
                item: that['hitTemplate'+mode]
            },
            transformData: function(hit) {
                try {
                    var avers = hit.images[Object.keys(hit.images)[0]];

                    hit.avers = false;
                    if (avers !== null && typeof avers === 'object') {
                        hit.avers = avers.name;
                        hit.avers_medium = avers.name.replace(/(\.[^.]*)$/, "_compact$1").replace('http:', '');
                        hit.avers_small = avers.name.replace(/(\.[^.]*)$/, "_small$1").replace('http:', '');
                    }
                } catch (e) {
                    hit.avers = false;
                }

                try {
                    var revers = hit.images[Object.keys(hit.images)[1]];

                    hit.revers = false;
                    if (revers !== null && typeof revers === 'object') {
                        hit.revers = revers.name;
                        hit.revers_medium = revers.name.replace(/(\.[^.]*)$/, "_compact$1").replace('http:', '');
                        hit.revers_small = revers.name.replace(/(\.[^.]*)$/, "_small$1").replace('http:', '');
                    }
                } catch (e) {
                    hit.revers = false;
                }

                if (typeof hit.caracteristics !== 'undefined' && typeof hit.caracteristics.en !== 'undefined') {
                    // quality
                    if (hit.caracteristics.en[37]) { // for coins
                        hit.quality = hit.caracteristics.en[37];
                    } else if (hit.caracteristics.en[30]) { // for banknotes
                        hit.quality = hit.caracteristics.en[30];
                    } else if (hit.caracteristics.en[208]) { // for medals
                        hit.quality = hit.caracteristics.en[208];
                    } else {
                        hit.quality = false;
                    }

                    // year
                    if (hit.caracteristics.en[29]) {
                        hit.year = hit.caracteristics.en[29];
                    } else {
                        hit.year = false;
                    }

                    // denomination
                    if (hit.caracteristics.en[6]) {
                        hit.denomination = hit.caracteristics.en[6];
                    } else {
                        hit.denomination = false;
                    }

                    // grading
                    if (hit.caracteristics.en[65]) {
                        hit.grading = hit.caracteristics.en[65];

                        if (hit.caracteristics.en[92]) {
                            hit.grading = hit.grading+' '+hit.caracteristics.en[92];
                        }
                    } else {
                        hit.grading = false;
                    }

                    // mintname
                    if (hit.caracteristics.en[42]) {
                        hit.composition = hit.caracteristics.en[42];
                    } else {
                        hit.composition = false;
                    }

                    // composition
                    if (hit.caracteristics.en[39]) {
                        hit.mintname = hit.caracteristics.en[39];
                    } else {
                        hit.mintname = false;
                    }
                }

                if (typeof hit._highlightResult !== 'undefined') {
                    hit.title = hit._highlightResult.translations.en.title.value;
                }

                that.products[hit.reference] = hit;

                return hit;
            }
        })
    );
}

AlgoliaSearch.prototype.initSearch = function () {
    this.search.addWidget(
        instantsearch.widgets.searchBox({
            container: '#searchInput',
            placeholder: 'Search a product'
        })
    );

    this.search.addWidget(
        instantsearch.widgets.pagination({
            container: '#pagination',
            cssClasses: {
                active: 'active',
                root: 'text-center hidden-xs',
            },
            labels: {
                previous: '<img src="https://cdn.shopify.com/s/files/1/0938/5674/t/5/assets/back.png" alt="<" class="back" /><span>Previous</span>',
                next: '<span>Next</span><img src="https://cdn.shopify.com/s/files/1/0938/5674/t/3/assets/next.png" alt="<" class="next" />'
            },
            showFirstLast: false
        })
    );

    this.search.addWidget(
        instantsearch.widgets.hitsPerPageSelector({
            container: '#perPageSelector',
            options: [
                {value: 24, label: '24 per page'},
                {value: 48, label: '48 per page'},
                {value: 96, label: '96 per page'}
            ]
        })
    );

    this.search.addWidget(
        instantsearch.widgets.stats({
            container: '#stats',
            templates: {
                body: '<strong>{{nbHits}}</strong> results</div>'
            }
        })
    );

    this.search.addWidget(
        instantsearch.widgets.priceRanges({
            container: '#priceChoice',
            attributeName: 'price',
            currency: '€',
            labels: {
                separator: 'to',
                button: 'Filter'
            },
            cssClasses: {
                list: 'nav nav-list',
                count: 'badge pull-right',
                active: 'active'
            },
            autoHideContainer: false
        })
    );

    this.search.addWidget(
        instantsearch.widgets.sortBySelector({
            container: '#sortBy',
            indices: [
                {name: 'products_custom', label: 'Default'},
                {name: 'products_en', label: 'Most relevant'},
                {name: 'products_price_asc', label: 'Lowest price'},
                {name: 'products_price_desc', label: 'Highest price'}
            ]
        })
    );

    this.search.addWidget(
        instantsearch.widgets.toggle({
            container: '#isSold',
            attributeName: 'isSold',
            label: 'Sold only',
            values: {
                on: true,
                off: false
            },
            cssClasses: {
                count: 'hide',
            }
        })
    );

    this.search.addWidget(
        instantsearch.widgets.clearAll({
            container: '#reset',
            templates: {
                link: 'Clear filters'
            },
            autoHideContainer: false
        })
    );

    this.initHit('Medium');
}

AlgoliaSearch.prototype.getAttributesForFaceting = function() {
    var dfd = jQuery.Deferred();
    var that = this;
    var client = algoliasearch(this.appId, this.apiKey);
    var index = client.initIndex(this.indexName);
    index.getSettings(function(err, content) {
        that.attributesForFaceting = content.attributesForFaceting;
        dfd.resolve(that.attributesForFaceting);
    });
    return dfd.promise();
}

AlgoliaSearch.prototype.getCaracteristics = function() {
    var that = this;
    var dfd = jQuery.Deferred();
    if (this.caracteristics == null) {
        $.ajax({
            url:  'https://placedesmonnaies.com/api/caracteristics',
            dataType: 'JSON',
        }).done(function(response) {
            that.caracteristics = response;
            dfd.resolve(that.caracteristics);
        }).fail(function(response) {
            dfd.reject();
        });
    }

    return dfd.promise();
}

AlgoliaSearch.prototype.toggleElements = function() {
    var that = this;
    // this method is called each time the DOM of #resultsContainer changes
    // so, we use a setTimeout to limit the number of times the method is executed
    clearTimeout(that.toggleTimer);

    that.toggleTimer = setTimeout(function(){
        $('[data-show]').each(function(){
            try{
                var display = eval($(this).attr('data-show'));
            } catch(e) {
                var display = true;
            }
            // display if "true" or anything else. Don't display if "false"
            if (display) {
                $(this).show();
            }
        });

        $('#facetsContainer .facetContainerToggle').each(function() {
            if ($(this).find('.ais-refinement-list--item').length == 0) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        //that.lazyload();
    }, 600);
}

AlgoliaSearch.prototype.lazyload = function() {
    $('img.lazy').lazyload({
        threshold : 500
    });
}

var AlgoliaSearchInstance = new AlgoliaSearch();
AlgoliaSearchInstance.init();

$(document).ready(function(){

    $('#facetsContainer').on('click', '.facetToggle', function() {
        $(this).parent().find('.contenu').toggle();
    });

    $('body').on('click', '.changeImage', function(e) {
        e.preventDefault();

        $('#imageDisplayMode').html($(this).html());
        $('.avers').toggle();
        $('.revers').toggle();
    });

    $('body').on('mouseover', '.produit:not(.big) .piece', function(e) {
        $(this).find('.avers').toggle();
        $(this).find('.revers').toggle();
    });

    $('body').on('mouseout', '.produit:not(.big) .piece', function(e) {
        $(this).find('.avers').toggle();
        $(this).find('.revers').toggle();
    });

    $('body').on('click', '.displayMode', function(e) {
        e.preventDefault();
        // hide the title to avoid visual bug
        $('#resultsContainer .produit h2').hide();

        $('.displayMode').removeClass('active');
        $(this).addClass('active');

        $('#resultsContainer').removeClass();
        $('#resultsContainer').addClass($(this).attr('data-class'));

        AlgoliaSearchInstance.initHit($(this).attr('data-mode'));
        AlgoliaSearchInstance.search.start();
    });

    $('#zoomHit').on('show.bs.modal', function (e) {
        var product = AlgoliaSearchInstance.products[e.relatedTarget.attributes['data-ref']['value']];

        $('#zoomHit [data-replace]').each(function(){
            $(this).html(product[$(this).attr('data-replace')]);
        });

        $('#zoomHit [data-src]').each(function(){
            $(this).attr('src', product[$(this).attr('data-src')]);
        });

        $('#zoomHit [data-link]').each(function(){
            $(this).attr('href', $(this).attr('data-link')+product['handle']);
        });

        $('#zoomHit [data-variant]').each(function(){
            $(this).attr($(this).attr('data-variant'), product['variant']);
        });

        $('#zoomHit [data-show]').each(function(){
            try{
                var display = eval(product[$(this).attr('data-show')]);
            } catch(e) {
                var display = true;
            }
            // display if "true" or anything else. Don't display if "false"
            if (!display) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        $('#zoomHit [data-wish]').each(function(){
            $(this).attr('value', 'WISH|'+product['handle'])
        });

        if ($(this).find('#error.nocustomer').length > 0) {
            $(this).find('#contactFormWrapper').hide();
            $(this).find('#success').hide();
            $(this).find('#error').show();
        } else {
            $(this).find('#contactFormWrapper').show();
            $(this).find('#success').hide();
        }
    });

    $('#zoomHit').on('click', '.miniature img', function(){
        $('#image-produit img').attr('src', $(this).attr('src'));
    });

    $('body').on('click', '.displayMode, .ais-price-ranges--link, .facet-item, .ais-refinement-list--item .css-label, .ais-toggle--checkbox, .ais-pagination--item, #reset', function(){
        AlgoliaSearchInstance.toggleElements();
    });

    $('body').on('change', '.ais-hits-per-page-selector, .ais-sort-by-selector', function(){
        AlgoliaSearchInstance.toggleElements();
    });

    $('body').on('keyup', '#searchInput', function(){
        AlgoliaSearchInstance.toggleElements();
    });


    $('body').on('submit', 'form[action="/apps/wish"]', function(e) {
        e.preventDefault();
        var form = $(this);

        $.ajax({
            url: form.attr('action'),
            type: 'GET',
            data: form.serialize(),
            success: function(response) {
              if (form.hasClass('little-form')) {
                $('#modalWishConfirm').modal('show');
              } else {
                form.find('#contactFormWrapper').hide();
                form.find('#success').show();
              }
            },
            error: function(XMLHttpRequest) {

            }
        });
        return false;
    });
});

$(window).load(function() {
    AlgoliaSearchInstance.toggleElements();

    setTimeout(function() {
        $('.facetContainerToggle .contenu').each(function() {
            if ($(this).find('.ais-refinement-list--item__active.active').length == 0) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    }, 400);

    //AlgoliaSearchInstance.lazyload();

    // prevent the search form to be submitted when "enter" is pressed
    $('#searchInput').unbind('keypress');

    $('#spinnerOverlay, #spinner, #spinnerMessage').hide();
});
